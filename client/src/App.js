import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Grid,
    Typography,
    Container,
    Link
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import moment from 'moment';
import css from './App.module.css';
import {StylesProvider} from '@material-ui/core/styles';

export default function App() {

    const [ posts, setPost ] = useState([]);

    const getPost = () => {

        axios.get(`http://localhost:8080/post`)
            .then(res => {

                let postList = [];

                res.data.map(item => {

                    item.trashShown = false;

                    item.timeToShow = moment(item.created_at).format('h:mm a');

                    let hours = moment('2020/06/13').diff(moment(item.created_at), 'hours');

                    if ((hours >= 0) && (hours <= 23)) item.timeToShow = 'Yesterday';

                    if (hours > 23) item.timeToShow = moment(item.created_at).format('MMM DD');

                    if (item.story_title === null) item.story_title = item.title;

                    if ((item.story_title !== null) || (item.title !== null)) postList.push(item);

                });

                setPost([ ...postList ]);
            }).catch(err => {
            console.log('ERROR: ', err);
        });

    };

    const hoverTrash = (index, value) => {

        const newPosts = posts;
        newPosts[index].trashShown = value;
        setPost([ ...newPosts ]);

    };

    const deleteTrash = (index) => {
        const newPosts = posts;
        const id = newPosts[index]._id;

        axios.put(`http://localhost:8080/post`, {id})
            .then(res => {

                newPosts.splice(index, 1);
                setPost([ ...newPosts ]);

            }).catch(err => console.err(err));
    };

    useEffect(() => {
        getPost();
    }, []);

    return (
        <>
            <StylesProvider injectFirst>
                <main>
                    {/* Hero unit */}
                    <div className={css.heroContent}>
                        <Container maxWidth="lg">
                            <Typography variant="h1" align="left" color="textPrimary" className={css.title}>
                                HN Feed
                            </Typography>
                            <Typography variant="h5" align="left" color="textSecondary" className={css.subtitle}>
                                We &lt;3 hacker news!
                            </Typography>
                        </Container>
                    </div>

                    <Container className={css.listContainer} maxWidth="lg">

                        <List>
                            {posts.map((item, index) => (
                                <ListItem key={index} className={css.listItem} divider
                                          onMouseEnter={() => hoverTrash(index, true)}
                                          onMouseLeave={() => hoverTrash(index, false)}>

                                    <ListItemText onClick={() => window.open(`${item.story_url}`, '_blank')}>
                                        <Grid container>
                                            <Grid item xs={10}>
                                                <Grid container spacing={1}>
                                                    <Grid item>
                                                        <Typography className={css.story_title}>
                                                            {item.story_title}
                                                        </Typography>

                                                    </Grid>

                                                    <Grid item>
                                                        <Typography className={css.author}>
                                                            - {item.author} -
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>

                                            <Grid item xs={2}>
                                                <Typography className={css.time}>
                                                    {item.timeToShow}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </ListItemText>

                                    <ListItemIcon onClick={() => deleteTrash(index)}>
                                        {item.trashShown && <DeleteIcon/>}
                                    </ListItemIcon>

                                </ListItem>
                            ))}
                        </List>

                    </Container>
                </main>
                {/* Footer */}
                <footer className={css.footer}>
                    <Typography variant="h6" align="center" gutterBottom>
                        @seijasce
                    </Typography>
                    <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                        <Link color="inherit" href="https://gitlab.com/seijasce/reign">
                            See the code at Gitlab
                        </Link>{' '}
                    </Typography>
                    <Typography variant="body1" color="textSecondary" align="center">
                        {'Copyright © '}
                        seijasce
                        {' '}
                        {new Date().getFullYear()}
                        {'.'}
                    </Typography>
                </footer>
                {/* End footer */}
            </StylesProvider>
        </>
    );
}