import {createMuiTheme} from '@material-ui/core';
import {blue, grey, orange} from '@material-ui/core/colors';

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: grey
    },
    status: {
        danger: orange
    }
});

export default theme;