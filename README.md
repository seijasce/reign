# Reign

This projects was developed using:
 - React
 - Express
 - Mongodb
 - Docker

## docker commands

- **images** 
list all images

- **image build** 
create images

- **run** 
start image

- **ps** 
list actived images

- **stop** 
stop --id

- **delete all** 
docker rmi -f $(docker images -a -q)

## docker-compose commands

- **build**: build docker images

- **up**: build docker images


## examples 

docker ps
docker images
docker stop id

docker run -e CI=true cliente npm run cliente

docker build -t cliente .
docker run -p 3000:3000 cliente

docker build -t reign_client client/.
docker run -p 3000:3000 reign_client

docker build -t reign_server server/.
docker run -p 8080:8080 reign_server

docker-compose build
docker-compose up
docker-compose down
docker-compose -f docker-compose.prod.yml up --build

## Getting Started

For start the project be shure to have docker and docker-compose installed, and run the command "docker-compose up --build". That command will start 3 images, a client (Create-react-app), a server (Express) and the database (mongoDB).

 You should go to this direction "http://localhost:80" you will see a page like this.

![alt text](https://gitlab.com/seijasce/reign/-/raw/master/readmeImages/start.png?raw=true)
 
 The first time you start the project you will have a empty database.
 You will need make a http request to the Back-end to forze the fill of the database at this direction ("http://localhost:8080/crontab")

you will received a message "the everything is ok".

![alt text](https://gitlab.com/seijasce/reign/-/raw/master/readmeImages/crontab.png?raw=true)

after that you can check the front

- For check if the front is up visit http://localhost:80/ and you will see a page like this

![alt text](https://gitlab.com/seijasce/reign/-/raw/master/readmeImages/front.png?raw=true)

- For check if the back is up visit http://localhost:8080/ and you will see a page like this

![alt text](https://gitlab.com/seijasce/reign/-/raw/master/readmeImages/back.png?raw=true)

- For check if the database is up make a http request to http://localhost:8080/post and you will see a page like this

![alt text](https://gitlab.com/seijasce/reign/-/raw/master/readmeImages/post.png?raw=true)

## Considerations

- Front End created with React and Material-UI
- Back End created with Express, Mongoose and MongoDB
- the project is initialized by a docker-compose.yml
- the project is build in node12
- the server and client has been Dockerized
