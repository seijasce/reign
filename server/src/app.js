'use strict';
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const routes = require('./routes/index');
const cron = require('node-cron');
const crontab = require('./controllers/crontab');
const app = express();

mongoose.connect('mongodb://mongo:27017/expressmongo',
    {useNewUrlParser: true, useUnifiedTopology: true}
).then(() => console.log('DB connected'))
    .catch((err) => console.log('ERROR', err));

let corsOptions = {
    origin: '*',
    methods: [ 'POST', 'PUT', 'GET', 'DELETE', 'OPTIONS' ],
    allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept'
};

// get data every hour
cron.schedule('59 59 * * * *', function () {
    crontab.crontab();
    console.log('running a task every hour');
});

app.use(cors(corsOptions));

app.use(morgan('dev'));

app.use(bodyParser.json());

app.use('/', routes);

app.listen(8080, () => {
    console.log(`Server at http://localhost:${8080}`);
});