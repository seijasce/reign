'use strict';

const express = require('express');
const routes = express.Router();
const path = require('path');
const crontab = require('../controllers/crontab');
const post = require('../controllers/post');

//init route
routes.get('/', (req, res) => {
    res.sendFile('index.html', {root: path.join(__dirname, '../assets')});
});

//crontab routes
routes.get('/crontab', crontab.crontab);

//post routes
routes.get('/post', post.getPost);
routes.get('/post/:id', post.specificPost);
routes.post('/post', post.createPost);
routes.put('/post', post.disablePost);
routes.delete('/post', post.deletePost);

module.exports = routes;
