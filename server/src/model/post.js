'use strict';

const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({

    objectID: {
        type: String,
        required: true,
        unique: true
    },
    title: {
        type: String
    },
    story_title: {
        type: String
    },
    story_url: {
        type: String
    },
    comment_text: {
        type: String,
    },
    author: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        default: false
    }

});

module.exports = mongoose.model('Post', PostSchema);

