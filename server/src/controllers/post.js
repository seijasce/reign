'use strict';

const Post = require('../model/post');

function getPost(req, res) {

    Post.find({
        $where: function () {
            return this.disabled === false;
        }
    }).sort({created_at: 'desc'})
        .then(data => res.status(200).send(data))
        .catch(err => res.status(500).err(err));
}

function specificPost(req, res) {

    const {id} = req.params;

    Post.findById(id)
        .then(data => res.status(200).send(data))
        .catch(err => res.status(500).err(err));
}

function createPost(req, res) {

    const {title, description} = req.body;

    const post = new Post({title, description});

    post.save()
        .then(data => res.status(200).send(data))
        .catch(err => res.status(500).err(err));
}

function disablePost(req, res) {

    const {id} = req.body;

    console.log(id);

    Post.updateOne({_id: id}, {
        $set: {disabled: true}
    })
        .then(data => res.status(200).send(data))
        .catch(err => res.status(500).err(err));
}

function deletePost(req, res) {

    const {id} = req.body;

    // Post.remove({_id: id})
    Post.remove()
        .then(data => res.status(200).send(data))
        .catch(err => res.status(500).err(err));
}

function createPostFromCrontab(title, story_title, comment_text, author, created_at, objectID, story_url) {

    const post = new Post({title, story_title, comment_text, author, created_at, objectID, story_url});

    Post.count({objectID}, (err, count) => {
        if (count === 0) {

            post.save()
                .then(data => console.log('insert OK'))
                .catch(err => console.log('ERROR', err));
        } else {
            console.log('this one exists');
        }
    });

}

module.exports = {
    getPost,
    specificPost,
    createPost,
    disablePost,
    deletePost,
    createPostFromCrontab
};