'use strict';
const axios = require('axios');
const post = require('./post');


function crontab(req, res) {

    const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

    axios.get(url
    ).then(data => {

        const hits = data.data.hits;

        hits.map(item => {

            const {title, story_title, comment_text, author, created_at, objectID, story_url} = item;

            post.createPostFromCrontab(title, story_title, comment_text, author, created_at, objectID, story_url);
        });

        if (res === undefined) {
            console.log('everything is ok');
        } else {
            res.status(200).send('everything is ok');
        }

    }).catch(err => res.status(200).send(err));

}

module.exports = {crontab};
